import {Failure} from './parsers/status.js';

export default class StringInterpreter {

  constructor() {
    this.parsers = [];
  }

  addParser(constructor) {
    this.parsers.push(constructor);
  }

  parse(input) {
    for (const Parser of this.parsers) {
      const parser = new Parser(input);
      const result = parser.parse();
      if (result.isOk()) {
        return result;
      }
    }
    return new Failure(input, 'no parser matched');
  }

}
