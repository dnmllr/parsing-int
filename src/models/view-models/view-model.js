import {observable} from 'mobx';

export default class ViewModel {

  static mappings = new Map();

  static registerTo(type, vm) {
    ViewModel.mappings.set(type, vm);
  }

  static make(value) {
    return new (ViewModel.mappings.get(value.constructor))(value);
  }

  @observable collapsed = true;

  initViewByType(value) {
    return ViewModel.make(value);
  }

  open() {
    this.collapsed = false;
  }

  close() {
    this.collapsed = true;
  }

  toggle() {
    this.collapsed = !this.collapsed;
  }
}
