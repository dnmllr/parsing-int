import {observable} from 'mobx';

import ViewModel from './view-model.js';

export default class PropertyViewModel extends ViewModel {

  @observable key = null;
  @observable value = null;

  constructor(key, value) {
    super();
    this.key = this.initViewByType(key);
    this.value = this.initViewByType(value);
  }

}
