import {observable} from 'mobx';

import ViewModel from './view-model.js';
import PropertyViewModel from './property.js'

export default class ObjectViewModel extends ViewModel {
  @observable obj = null;
  @observable properties = [];

  constructor(obj) {
    super();
    this.obj = obj;
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) {
        this.properties.push(new PropertyViewModel(key, obj[key]));
      }
    }
  }
}

ViewModel.registerTo(Object, ObjectViewModel);
