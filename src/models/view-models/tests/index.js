import ObjectViewModel from '../object.js';

const vm = new ObjectViewModel({
  a: 5,
  b: 'hello',
  c: [1,2,3],
  d: {
    e: 12937
  }
});

console.log(vm);
