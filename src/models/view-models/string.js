import {observable} from 'mobx';

import ViewModel from './view-model.js';

export default class StringViewModel extends ViewModel {
  @observable str = null;

  constructor(str) {
    super();
    this.str = str;
  }
}

ViewModel.registerTo(String, StringViewModel);
