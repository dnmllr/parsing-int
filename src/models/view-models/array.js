import {observable} from 'mobx';

import ViewModel from './view-model.js';


export default class ArrayViewModel extends ViewModel {
  @observable arr = null;
  @observable children = [];

  constructor(arr) {
    super();
    this.arr = arr;
    for (const val of arr) {
      this.children.push(this.initViewByType(val));
    }
  }

}

ViewModel.registerTo(Array, ArrayViewModel);
