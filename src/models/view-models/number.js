import {observable} from 'mobx';

import ViewModel from './view-model.js';

export default class NumberViewModel extends ViewModel {
  @observable num = null;

  constructor(num) {
    super();
    this.num = num;
  }
}

ViewModel.registerTo(Number, NumberViewModel);
