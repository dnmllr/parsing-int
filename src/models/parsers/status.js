export default class Status {

  static specialize({output, comment}) {
    return new this(output, comment);
  }

  constructor(status, output, comment) {
    this.status = status || false;
    this.output = output || '';
    this.comment = comment || '';
  }

  isOk() {
    return !!this.status;
  }

  clone() {
    return Status.specialize.call(this.constructor, this);
  }

}

export class Failure extends Status {

  constructor(output, comment) {
    super(false, output, comment);
  }

}

export class Success extends Status {

  constructor(output, comment) {
    super(true, output, comment);
  }

}
