import Parser from './parser.js';
import {Success, Failure} from './status.js';

export default class RubyParser extends Parser {

  constructor(input) {
    super(input);
  }

  toString() {
    return 'Ruby';
  }

  parseKey() {
    this.skipWhitespace();
    if (!this.expect(':')) {
      return new Failure(this.peek(), 'expected to see : starting a symbol')
    }
    this.get();
    return this.parseSymbol();
  }

  parseArrow() {
    if (this.isDone()) {
      return new Failure('', 'nothing to parse');
    }
    this.skipWhitespace();
    if (!this.expect('=')) {
      return new Failure(this.peek(), 'arrows start with =');
    }
    this.get();
    if (!this.expect('>')) {
      return new Failure(this.peek(), 'arrows end with >');
    }
    this.get();
  }

  parseValue() {
    let result;
    this.skipWhitespace();
    result = this.attempt(() => this.parseNumber());
    if (result.isOk()) {
      return result;
    }
    result = this.attempt(() => this.parseString());
    if (result.isOk()) {
      return result;
    }
    result = this.attempt(() => this.parseKey());
    if (result.isOk()) {
      return result;
    }
    result = this.attempt(() => this.parseHash());
    if (result.isOk()) {
      return result;
    }
    return new Failure(this.get(), 'unable to parse');
  }

  parseHash() {
    const result = {};
    let sawEnd = false;

    this.skipWhitespace();
    if (!this.expect('{')) {
      return new Failure(this.peek(), 'hashes open with {');
    }
    this.get();
    this.skipWhitespace();
    while (!this.isDone()) {

      const key = this.parseKey();
      if (!key.isOk()) {
        return key;
      }

      this.parseArrow();

      const value = this.parseValue();
      if (!value.isOk()) {
        return value;
      }

      result[key.output] = value.output;
      this.skipWhitespace();

      if (this.expect('}')) {
        sawEnd = true;
        this.get();
        this.skipWhitespace();
        break;
      } else if (!this.expect(',')) {
        return new Failure(result, 'no comma');
      }
      this.get();
    }

    if (sawEnd) {
      return new Success(result);
    } else {
      return new Failure(result, 'didn\'t close hash');
    }
  }

  parse() {
    return this.parseValue();
  }


}
