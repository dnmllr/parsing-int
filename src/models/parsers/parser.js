import {Success, Failure} from './status.js';

export default class Parser {

  constructor(input) {
    this.input = input;
    this._i = 0;
    this._rollback = 0;
  }

  peek() {
    return this.input[this._i];
  }

  get() {
    return this.input[this._i++];
  }

  isDone() {
    return this._i >= this.input.length;
  }

  skipWhitespace() {
    while (/\s/.test(this.peek())) {
      this.get();
    }
  }

  expect(...cs) {
    return cs.reduce((ok, c) => ok || this.peek() === c, false);
  }

  setRollback() {
    this._rollback = this._i;
  }

  rollback() {
    this._i = this._rollback;
  }

  attempt(cb) {
    if (this.isDone()) {
      return new Failure('', 'nothing to parse');
    }

    this.setRollback();

    const result = cb();

    if (!result.isOk()) {
      this.rollback();
    }

    return result;
  }

  parseSymbol() {
    let sym = '';
    if (this.isDone()) {
      return new Failure(sym, 'nothing to parse');
    }
    if (!/[A-Za-z]/.test(this.peek())) {
      return new Failure(sym, 'symbol didn\'t begin with letter');
    }
    sym += this.get();
    while (/[A-Za-z0-9]/.test(this.peek())) {
      sym += this.get();
    }
    return new Success(sym);
  }

  attemptParseNumber() {
    return this.attempt(() => this.parseNumber());
  }

  parseNumber() {
   /*
    * TODO(Dan) make this support hex and octal...
    * or maybe not, maybe another method parseHex
    */
    if (this.isDone()) {
      return new Failure(sym, 'nothing to parse');
    }
    let number = '';
    while (/\d/.test(this.peek())) {
      number += this.get();
    }
    return !number ? new Failure(this.peek()) : new Success(Number(number));
  }

  attemptParseString() {
    return this.attempt(() => this.parseString());
  }

  // TODO(dan) not all languages allow for single or double quoted strings
  // don't combine cases here
  parseString() {
    let str = '';
    let sep = null;
    if (this.isDone()) {
      return new Failure(sym, 'nothing to parse');
    }
    if (this.expect('\'')) {
      sep = this.get();
    } else if (this.expect('"')) {
      sep = this.get();
    } else {
      return new Failure(this.peek(), 'not a string');
    }
    while (this.peek() !== sep) {
      str += this.get();
    }
    if (!this.expect(sep)) {
      return new Failure(str, 'unclosed string');
    }
    this.get();
    return new Success(str);
  }

  parse() {
    return new Failure(this.input, 'default');
  }

}
