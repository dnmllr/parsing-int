import Parser from '../parser.js';

var x = new Parser('    ');
x.skipWhitespace();
console.log('skips whitespace', x.isDone());

x = new Parser('   23432   ');
x.skipWhitespace();
var {
  status,
  output
} = x.parseNumber();
x.skipWhitespace();
console.log('skips whitespace then number then whitespace', x.isDone());
console.log('parsed number successfully', status && output === 23432);

x = new Parser('   "this is a string"   ');
x.skipWhitespace();
var {
  status,
  output
} = x.parseString();
x.skipWhitespace();
console.log('skips whitespace then string then whitespace', x.isDone());
console.log('parsed string successfully', status && output === 'this is a string');

x = new Parser('    Symbol123    ');
x.skipWhitespace();
var {
  status,
  output
} = x.parseSymbol();
x.skipWhitespace();
console.log('skips whitespace then symbol then whitespace', x.isDone());
console.log('parsed symbol successfully', status && output === 'Symbol123');
