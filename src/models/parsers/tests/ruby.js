import RubyParser from '../ruby-parser.js';

var x = new RubyParser('');
var {
  status,
  output
} = x.parse();
console.log('ruby parser shouldn\'t parse empty', !status);

x = new RubyParser('{:a=>5}');
var {status, output} = x.parse();
console.log('ruby parser should parse hashes successfully', status);
console.log('ruby parser should parse values', output.a === 5);
