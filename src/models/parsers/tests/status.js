import Status, {Success, Failure} from '../status.js';

const x = new Status(false, 'output', 'comment');
console.log('status clones', x.clone().constructor === Status);
const succ = new Success('output', 'comment');
console.log('success clones', succ.clone().constructor === Success)
console.log('success should be ok', succ.isOk());
const failure = new Failure('output', 'comment');
console.log('failure clones', failure.clone().constructor === Failure);
console.log('failure should not be ok', !failure.isOk());
