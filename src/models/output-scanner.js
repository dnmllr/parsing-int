import {observable} from 'mobx';
import ViewModel from './view-models/view-model';

export default class OutputScanner {

  @observable outputs = [];

  constructor(interpreter) {
    this.interpreter = interpreter;
  }

  receive(input) {
    const result = this.interpreter.parse(input);
    if (result.isOk()) {
      this.output.push(ViewModel.make(result.output));
    } else {
      this.output.push(ViewModel.make(input));
    }
  }

}
