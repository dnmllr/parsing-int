import StringInterpreter from '../models/string-interpreter.js';
import * as parsers from '../models/parsers';

const interpreter = Object.values(parsers).reduce((inter, parser) => {
  inter.addParser(parser);
  return inter;
}, new StringInterpreter());

export default interpreter;
