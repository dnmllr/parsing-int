import React from 'react';
import {observer} from 'mobx-react';
import styles from './main.styl';

@observer
export default class Main extends React.Component {

  constructor(props) {
    super(props);
    const {interpreter, OutputScanner} = props;
    this.state = {
      scanner: new OutputScanner(interpreter),
    };
  }

  componentWillReceiveProps({interpreter, OutputScanner}) {
    if (this.props.OutputScanner !== OutputScanner) {
      this.setState({
        scanner: new OutputScanner(interpreter),
      });
    }
  }

  render() {
    if (this.state && this.state.scanner) {
      return (
        <div className={styles.output}>
          {this.state.scanner.outputs.map((output) => (
            <div className={styles.main}>
              {JSON.stringify(output)}
            </div>
          ))}
        </div>
      );
    } else {
      return <div>No output</div>;
    }
  }

}
