import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/main/main.jsx';

import interpreter from './concretions/interpreter.js';
import OutputScanner from './models/output-scanner.js';

// TODO(Dan) This is all super ugly. The view model instantiation and registration
// Definitely a better way to do this.
import './models/view-models';

// Run the tests in the console
import './run-tests.js';


ReactDOM.render(<Main interpreter={interpreter}
                      OutputScanner={OutputScanner} />, document.getElementById('app'));

