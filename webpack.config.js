const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, './src/app.jsx'),
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'replit.interview.bundle.js'
  },
  devtool: 'eval-source-map',
  module: {
    loaders: [{
      test: /(\.js|\.jsx)$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    }, {
      test: /\.css$/,
      use: ['style-loader', {
        loader: 'css-loader',
        options: {
          modules: true,
          localIdentName: '[name]__[local]__[hash:base64:5]'
        }
      }]
    }, {
      test: /\.styl$/,
      use: ['style-loader', {
        loader: 'css-loader',
        options: {
          modules: true,
          localIdentName: '[name]__[local]__[hash:base64:5]'
        }
      }, 'stylus-loader']
    }]
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true
  },
  plugins: [new HtmlWebpackPlugin({
    template: path.resolve(__dirname, './src/index.html'),
    filename: 'index.html',
    inject: 'body'
  })]
};
